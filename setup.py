from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='utils',
      version='0.1',
      description='Helpful utilities',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6'
      ],
      keywords='utils',
      url='http://github.com/jamie-r-davis/utils',
      author='Jamie Davis',
      author_email='jamjam@umich.edu',
      license='MIT',
      packages=['utils'],
      install_requires=['jinja2', 'html2text', 'selenium'],
      entry_points={},
      include_package_data=True,
      zip_safe=False)
