import os
import sys
from zipfile import ZipFile


def splitfile(filepath, chunks, headers=True, **kwargs):
    """
    Splits a file into chunks, preserving header row if set.

    The split files will be written to the same directory as the source file
    and have the chunk number appended to the filename:

    >>> splitfile(foo.txt, 3)
    foo-001.txt
    foo-002.txt
    foo-003.txt

    Parameters
    ----------
    filepath : str
        The path to the file.
    chunks : int
        The number of chunks to split the file into.
    headers : bool
        Whether there is a header row (default: True).

    Kwargs
    ------
    encoding : str
        The encoding to use when opening the file. By default, utf-8 is used.

    Returns
    -------
    out_files : list
        Array of the filenames generated.
    """

    encoding = kwargs.get('encoding', 'utf8')


    # read from source file
    with open(filepath, encoding=encoding) as infile:
        if headers:
            header_row = infile.readline()
        data_rows = infile.readlines()

    if chunks < 1:
        raise ValueError(f'Cannot split file into {chunks} chunks.')
    elif chunks > len(data_rows):
        chunks = len(data_rows)
        chunk_size = 1
    else:
        chunk_size = len(data_rows) // chunks + 1

    # process chunks
    outfiles = []
    for i, j in enumerate(range(0, len(data_rows), chunk_size)):
        outname = f'{filepath[:-4]}-{i+1:0>3}.{filepath[-3:]}'
        with open(outname, 'w', encoding=encoding) as outfile:
            chunked_data = data_rows[j:j+chunk_size]
            if headers:
                outfile.write(header_row)
            outfile.writelines(chunked_data)
        outfiles.append(outname)
    return outfiles


def splitzip(filename, chunks):
    """
    Split a zip archive into n chunks.

    The output will be generated in the same directory as the source file with
    the chunk number appended to the filename:

    >>> splitzip('foo.zip', 3)
    ['foo-001.zip', 'foo-002.zip', 'foo-003.zip']

    Parameters
    ----------
    filename : str
        The path to the zip file.
    chunks : int
        The number of chunks to split the file into.

    Returns
    -------
    zip_files : list
        An array of the filenames generated.
    """
    abs_path = os.path.abspath(filename)
    file_dir = os.path.dirname(abs_path)
    basename = os.path.basename(abs_path)
    fname, ext = os.path.splitext(basename)

    outfiles = []
    with ZipFile(filename, 'r') as infile:
        files = infile.namelist()
        if chunks > len(files):
            chunks = len(files)
            print(f'{filename} contains {len(files)} files. '\
                  f'Splitting into {chunks}.')
        if len(files) < 1 or chunks < 1:
            raise AttributeError(
                f'Cannot split {len(files)} files into {chunks} chunks.')
        for i in range(chunks):
            outname = os.path.join(file_dir, f"{fname}-{i+1:0>3}{ext}")
            with ZipFile(outname, 'w') as outfile:
                for f in files[i::chunks]:
                    outfile.writestr(f, infile.read(f))
            outfiles.append(outname)
    return outfiles
