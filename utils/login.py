from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException


def login(driver, username, password):
    user = driver.find_element_by_id('login')
    user.clear()
    user.send_keys(username)
    pwd = driver.find_element_by_id('password')
    pwd.clear()
    pwd.send_keys(password)
    driver.find_element_by_id('loginSubmit').click()
    print('Waiting for DUO authentication...')
    try:
        WebDriverWait(driver, 180).until(
            lambda d: d.title != 'U-M Weblogin')
    except TimeoutException:
        print('Login failed')
        raise
    else:
        print('Login successful.')
