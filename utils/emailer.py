import html2text
import logging
import os
import re
import smtplib

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from jinja2 import Environment, FileSystemLoader, PackageLoader


def send_email(subject, msg, to, username, password,
               host='smtp.gmail.com', port=587, bcc=[]):
    """
    Send simple plaintext email.

    Params
    ------
    subject : str
        The subject line of the email
    msg : str
        The plaintext message for the email
    to : list
        A list of recipients
    username : str
        The login username, also the sending account.
    password : str
        The password
    host : str
        The smtp host (eg. smtp.gmail.com)
    port : int
        The port (default: 587)
    bcc : list
        A list of bcc recipients
    """
    if isinstance(to, str):
        to = [to]
    to_recipients = ', '.join(to)
    all_recipients = to + bcc
    message = f"""
From: {username}
To: {to_recipients}
Subject: {subject}

{msg}""".strip()
    try:
        server = smtplib.SMTP(host, port)
        server.ehlo()
        server.starttls()
        server.login(username, password)
        server.sendmail(username, all_recipients, message)
        server.close()
        print('email sent')
    except:
        print("email failed to send")


class Emailer(object):
    """
    SMTP Email object.
    """

    HTML_REGEX = re.compile(r'(^<!DOCTYPE html.*?>)')

    def __init__(self, smtp_host, username, password, from_user=None,
                 template_dir='templates', smtp_port=587):
        logging.basicConfig()
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        self.smtp_host = smtp_host
        self.smtp_port = smtp_port
        if from_user:
            self.from_user = from_user
        else:
            self.from_user = username
        self.reply = self.from_user
        self.username = username
        self.password = password
        if os.path.isdir(template_dir):
            self.template_dir = template_dir
        else:
            self.template_dir = os.path.join(
                os.path.abspath(os.path.dirname(__file__)), template_dir)
        self.logger.debug(f"Setting template directory to {self.template_dir}")
        self.env = Environment(loader=FileSystemLoader(self.template_dir))

    def _mailrender(self, template, data):
        """
        Render a template with the data provided.
        """
        self.logger.debug(f'Rendering template "{template}"')
        text = self.env.get_template(template)
        msg = text.render(data)
        return msg

    def _smtpconnect(self):
        """
        Connect/auth to the SMTP server, returning the connection object.
        """
        try:
            smtp = smtplib.SMTP(self.smtp_host, self.smtp_port)
        except Exception as e:
            self.logger.error(f'Cannot connect with "{self.smtp_host}": {e}')
            raise
        if self.username:
            try:
                smtp.ehlo()
                smtp.starttls()
                smtp.login(self.username, self.password)
            except smtplib.SMTPException as e:
                self.logger.error(f'Cannot auth with "{self.username}" '
                                  f'on {self.smtp_host}: {e}')
                raise
        return smtp

    def _smtpsend(self, conn, recipients, subject, html=[], text='', bcc=[]):
        all_recipients = recipients + bcc
        if self.HTML_REGEX.match(html) is None:
            self.logger.debug("Sending plaintext mail.")
            msg = MIMEText(text)
        else:
            self.logger.debug("Sending HTML mail.")
            msg = MIMEMultipart('alternative')
            if text != '':
                msg.attach(MIMEText(text, 'text'))
            else:
                msg.attach(MIMEText(html2text.html2text(html), 'text'))
            msg.attach(MIMEText(html, 'html', 'utf-8'))

        msg['From'] = self.from_user
        msg['To'] = ', '.join(recipients)
        msg['Reply-to'] = self.reply
        msg['Subject'] = subject
        conn.sendmail(self.from_user, all_recipients, msg.as_string())
        conn.close()

    def send_mail(self, recipients, subject, html='', text='', bcc=[]):
        """
        Send an email message.
        """
        conn = self._smtpconnect()
        try:
            self._smtpsend(conn, recipients, subject, html, text, bcc)
        except smtplib.SMTPException as e:
            self.logger.error("Cannot send mail.")

    def send_template(self, recipients, subject, template, data, bcc=[]):
        if recipients is None:
            error = "No recipients provided."
            self.logger.error(error)
            raise ValueError(error)
        msg = self._mailrender(template, data)
        self.send_mail(recipients, subject, msg, text='', bcc=bcc)
