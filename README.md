# utils

A handful of useful utilities for everyday Python.


## utils.emailer

### send_mail

Simple function to send plaintext email.

```python
send_mail('subject', 'message', 'recipient@example.com', 'sender@example.com', 'secret-password')
# email sent
```

### Emailer
A class for sending emails, allowing you to send emails quickly with `Emailer.send_mail()` or to render from a template with `Emailer.send_template()`.


## utils.splitfile

### splitfile

Quickly split a data file into equal-sized chunks.

```python
splitfile('foo.txt', 3)
# ['foo-001.txt', 'foo-002.txt', 'foo-003.txt']
```

### splitzip

Quickly split a zip file into equal-sized chunks.

```python
splitzip('foo.zip', 3)
# ['foo-001.zip', 'foo-002.zip', 'foo-003.zip']
```

## utils.login

### login
Quick U-M authentication in a pinch.

```python
d = webdriver.Chrome()
d.get('https://weblogin.umich.edu')
login(d, 'username', 'password')
# Waiting for duo authentication...
# Login successful
```
